# frozen_string_literal: true

lib = File.expand_path("lib", __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

Gem::Specification.new do |s|
  s.name = "shared-testing"
  s.version = "0.1.0"
  s.authors = ["Nivelat"]
  s.summary = "Common configuration for Minitest and Dummy application"
  s.description = "Common configuration for Minitest and Dummy application"

  s.files = Dir["{config,lib}/**/*", "Rakefile"]

  # Core
  s.add_dependency "rails", "~> 5.1"
  s.add_dependency "minispec-rails"

  # Tools
  s.add_dependency "combustion", "~> 1.3"
  s.add_dependency "fakeredis"
  s.add_dependency "minitest-stub_any_instance"
  s.add_dependency "minitest-hooks"
  s.add_dependency "mocha"
  s.add_dependency "policy-assertions"
  s.add_dependency "rails-controller-testing"
  s.add_dependency "simplecov"
  s.add_dependency "test-prof"
  s.add_dependency "webmock"
  s.add_dependency "wisper-minitest"
  s.add_dependency "dotenv-rails"

  # Formatters
  # Progressbar-like formatter for Minitest
  s.add_dependency "minitest-reporters"

  # Internal
  s.add_development_dependency "pry", "~> 0.12.2"
  s.add_development_dependency "pry-byebug", "3.5.1"
  s.add_development_dependency "byebug"
  s.add_development_dependency "rubocop-rails"
end
