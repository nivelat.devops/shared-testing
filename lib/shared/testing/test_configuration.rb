require 'rails/test_help'
require 'minispec/rails'
require 'minitest/autorun'
require 'minitest/mock'
require 'minitest/stub_any_instance'
require 'minitest/reporters'
require 'rails-controller-testing'
require 'dotenv'
require 'byebug'

if ENV['SPEC']
  Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new]
else
  Minitest::Reporters.use! [Minitest::Reporters::ProgressReporter.new]
end

# Filter out Minitest backtrace while allowing backtrace from other libraries
# to be shown.
# Minitest.backtrace_filter = Minitest::BacktraceFilter.new

Rails::Controller::Testing.install

# Define whether we're within engine or app
root = defined?(ENGINE_ROOT) ? Pathname.new(ENGINE_ROOT) : Rails.root

# Load fixtures from the app or engine
if ActiveSupport::TestCase.respond_to?(:fixture_path=)
  ActiveSupport::TestCase.fixture_path = root.join('test/fixtures')
  ActionDispatch::IntegrationTest.fixture_path = ActiveSupport::TestCase.fixture_path
  ActiveSupport::TestCase.file_fixture_path = ActiveSupport::TestCase.fixture_path + '/files'
end

# Load test env variables
Dotenv.load(root.join('.env.test'))

# Load helpers
Dir[File.join(__dir__, 'helpers/**/*.rb')].each { |f| require f }

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...

  def log_time
    start_at = Time.current

    yield if block_given?

    execution_time = (Time.current - start_at).round(2)
    puts "Time: #{execution_time}s"
  end
end

require 'mocha/minitest'
require 'wisper/minitest/assertions'
