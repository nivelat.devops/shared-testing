module Minitest
  module Assertions
    ##
    # Fails unless there is a Pundit policy for the user and record and permits
    # the action.
    # user: instance of User
    # record: instance or class of object being authorized
    # action: string or symbol of the action being authorized or array of
    #         strings/ symbols

    def assert_permit(user, record, actions)
      actions = Array(actions)

      actions.each do |action|
        msg = "User #{user.inspect} should be permitted to #{action} #{record},
          but isn't permitted"
        assert Pundit.policy!(user, record).public_send(action), msg
      end
    end

    ##
    # Same as `assert_permit` except it fails if user IS permitted for the
    # record and action

    def refute_permit(user, record, actions)
      actions = Array(actions)

      actions.each do |action|
        msg = "User #{user.inspect} should NOT be permitted to #{action} #{record},
          but is permitted"
        assert_not Pundit.policy!(user, record).public_send(action), msg
      end
    end
  end
end
