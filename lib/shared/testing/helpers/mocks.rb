module Minitest
  module Assertions
    def expect_mock_not_called(mock)
      mock.verify
    rescue MockExpectationError
      assert true
    else
      assert false
    end
  end
end
