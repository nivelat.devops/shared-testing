 module PaginationHelper
  def paginated_records(response)
    total_count = response.headers['X-Total-Count']
    pages = response.headers['X-Total-Pages']

    return total_count, pages
  end
end
