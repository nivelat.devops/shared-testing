# Shared Testing

Minitest configurations and utils for usage in gems, engines and apps.

## Installation

Add this line to your application's Gemfile:

```ruby
gem "shared-testing", git: 'https://gitlab.com/nivelat.devops/shared-testing.git', tag: 'version-tag'
```

or to your engine's Gemfile or `.gemspec`:

```ruby
# Gemfile
gem "shared-testing", , git: 'https://gitlab.com/nivelat.devops/shared-testing.git', tag: 'version-tag'

# .gemspec
s.add_development_dependency "shared-testing"
```

## Usage

This gem provides configurations, which could be loaded via:
- `require "shared/testing/test_configuration"` – this is Minitest core configuration,
which must be added to your `test_helper.rb`; it doesn't contain anything Rails specific
- create a .env.test file, to include env variables used by tests
- include helpers in test_helper.rb if necessary (see **Helpers**)

### Tools

Here is the list of gems included into Rails config:
- [minispec-rails](https://github.com/ordinaryzelig/minispec-rails)
- [combustion](https://github.com/pat/combustion)
- [fakeredis](https://github.com/guilleiguaran/fakeredis)
- [rails-controller-testing](https://github.com/rails/rails-controller-testing)
- [minitest-stub_any_instance](https://github.com/codeodor/minitest-stub_any_instance)
- [minitest-reporters](https://www.rubydoc.info/github/kern/minitest-reporters/master)
- [minitest-hooks](https://github.com/jeremyevans/minitest-hooks)
- [policy-assertions](https://github.com/ProctorU/policy-assertions)
- [simplecov](https://rubydoc.info/gems/simplecov/frames)
- [mocha](https://mocha.jamesmead.org/)
- [test-prof](https://test-prof.evilmartians.io/#/)
- [webmock](https://github.com/bblimke/webmock)
- [wisper-minitest](https://github.com/digitalcuisine/wisper-minitest)
- [dotenv-rails](https://github.com/bkeepers/dotenv)

You can also add a dummy Rails app (in case of gem/engine) using [`combustion`](https://github.com/pat/combustion) gem, which is included, too. For example:

```ruby
require "shared/testing/test_configuration"

require "combustion"

Combustion.initialize! :action_controller, :active_record, :active_job, :action_mailer do
  config.logger = Logger.new(nil)
  config.log_level = :fatal

  config.active_record.raise_in_transactional_callbacks = true

  config.active_job.queue_adapter = :test
end
```

Additional configuration could be added under `test/internal`, inside engine.

### Helpers

**Pundit:**
  - assert_permit(user, record, actions):  Assertion to test access from users to actions. Fails unless there is a Pundit policy for the user and record and permits.
    - Parameters
      * user: instance of User
      * record: instance or class of object being authorized
      * action: string or symbol of the action being authorized or array of strings/ symbols
  - refute_permit(user, record, actions): Same as `assert_permit` except it fails if user IS permitted for the record and action.

**Mocks:**
  - expect_mock_not_called(mock): Assertion to test if mock is been used. Fails unless mock is not called inside stubbed block.
    - Parameters
      * mock: Mock.new object

**PaginationHelper:**
  
  Must be included in test_helper:
  ```
  class ActiveSupport::TestCase
    include PaginationHelper
    ...
  ```

  - paginated_records(response): Helper method to return total count and total pages of paginated response. It returns two values (count and pages), so must be used like this: 
    ```count, pages = paginated_records(response) ```
    - Parameters
      * response: Response from http request
